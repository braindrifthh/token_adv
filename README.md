### Features

Make [Custom Tokens](https://www.drupal.org/project/token_custom)
exportable via features.

Tokens can be altered just by setting a system variable
`$conf['tokens_alter']['[custom:request-email]'] = 'info@example.com';`

New global tokens (`site:base_url`, `site:base_path` and `site:base_root`) are provided.
This tokens can be used to generate URL's. This new tokens are neede because on
a multilingual site under the URL `http://example.com?lang=de`
a token `[site:url]content` would result in a path `http://example.com?lang=decontent`
(sometimes for anonymous users only, depends on settings made on
`Configuration > Langages > Detection and selection`).

It also provides a global dynamic token `[site:path:?]`. This token generates an
absolute URL for internal paths. This can be used to generate a path that is
compatible with multilingual sites which use a request/session parameter
detection mode.

#### Example:
The token `[site:path:company/contact?subject=Price request]` on the page
`http://example.com/about?lang=de` would generate the URL
`http://example.com/company/contact?subject=Price%20request&lang=de`
for anonymous users.